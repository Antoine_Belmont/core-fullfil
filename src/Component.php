<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Moreno\Core\Fullfill;

use Zend\ServiceManager\ServiceManager;
use Doctrine\ORM\EntityManager;
use Moreno\Core\Fullfill\Entity\ZipCodes;

/**
 * Description of Command
 *
 * @author dev
 */
 
class Component
{
    
    /**
     *
     * @var type 
     */
    protected $services;

    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services)
    {
        $this->services = $services;
        $this->em = $services->get(EntityManager::class);
    }
    
    /**
     * 
     * @return type
     */    
    public function fetchAll()
    {
        return $this->em->getRepository(ZipCodes::class)->fetchAll();
    }      
}

